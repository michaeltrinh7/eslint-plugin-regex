//  Copyright (c) 2020 Gonzalo Müller Bravo.
//  Licensed under the MIT License (MIT), see LICENSE.txt

const optionsUtils = require('../utils/options-utils.js')

function escapeRegExp(string) {
  return string.replace(/[.*+\-?^${}()|[\]\\]/g, '\\$&'); // $& means the whole matched string
}
function replaceAll(str, find, replace) {
  return str.replace(new RegExp(escapeRegExp(find), 'g'), replace);
}
function checkRegex(sourceLines, regex, errMsg, report, regexSource) {
  for(var line = 0 ; line<sourceLines.length ; line++) {
	const aline = sourceLines[line];
	let matches = regex.exec(aline);
	
	if(matches && matches.length > 0){
		var err = errMsg;
		for(var i=0;i<matches.length;i++){
			err = replaceAll(err, '{'+i+'}', matches[i]);
		}
		report({
		  loc: {
			start: {
			  line: (line+1),
			  column: aline.search(regex)
			}
		  },
		  message: err,
		})
	}
	
  }
}
function checkPatterns(source, patterns, report, node) {
  const sourceLines = source.split('\n')
  patterns.map(patern => checkRegex(
    sourceLines,
    patern.regex,
	patern.errMsg,
    report,
    patern.source
  ));
}

module.exports = {
  meta: {
    type: 'suggestion',
    docs: {
      description: '..',
      category: 'Stylistic Issues',
    },
    schema: [{
      title: 'Invalid regular expressions',
      description: 'Invalid regular expressions to be reported',
      type: 'object',
      properties: {
		patterns: {
		  type: 'object'
		}
	  }	
    }, {
      title: 'Ignore file pattern',
      description: 'Pattern',
      type: 'string'
    }]
  },
  create: function (context) {
    const options = optionsUtils.fromOptions(context.options)
    return {
      Program: function (node) {
        if (!options.ignoreFilePattern.test(context.getFilename())) {
          checkPatterns(context.getSourceCode(node).getText(), options.patterns, context.report, node)
        }
      }
    }
  }
}
