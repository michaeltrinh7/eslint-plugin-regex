//  Copyright (c) 2020 Gonzalo Müller Bravo.
//  Licensed under the MIT License (MIT), see LICENSE.txt
module.exports = {
  fromOptions: function (options) {
	const patterns = options[0].patterns;
	const keys = Object.keys(patterns);
    return {
      ignoreFilePattern: new RegExp(options[1] || '^$'),
      patterns: keys.map(key => ({
        source: key.trim(),
        regex: new RegExp(key.trim(), 'gm'),
		errMsg: patterns[key]
      }))
    }
  }
}
